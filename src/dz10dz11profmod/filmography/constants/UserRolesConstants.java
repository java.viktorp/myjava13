package com.filmography.constants;

public interface UserRolesConstants {
    String ADMIN = "ADMIN";
    String MANAGER = "MANAGER";
    String USER = "USER";
}
