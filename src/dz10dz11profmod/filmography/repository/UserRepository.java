package com.filmography.repository;

import com.filmography.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends GenericRepository<User> {
    User findUserByLogin(String login);
    User findUserByEmail(String email);
    User findUserByChangePasswordToken(String token);

    @Query(nativeQuery = true,
            value = """
                 select u.*
                 from users u
                 where u.first_name ilike '%' || coalesce(:firstName, '%') || '%'
                 and u.last_name ilike '%' || coalesce(:lastName, '%') || '%'
                 and u.login ilike '%' || coalesce(:login, '%') || '%'
                  """)
    Page<User> searchUsers(String firstName,
                           String lastName,
                           String login,
                           Pageable pageable);
    @Query(nativeQuery = true,
            value = """
                 select email
                 from users u join film_rent_info fri on u.id = fri.user_id
                 where fri.return_date >= now()
                 and fri.returned = false
                 and u.is_deleted = false
                 """)
    List<String> getDelayedEmails();
}
