package com.filmography.repository;

import com.filmography.model.Director;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
    Page<Director> findAllByIsDeletedFalse(Pageable pageable);

    Page<Director> findAllByFioContainsIgnoreCaseAndIsDeletedFalse(String fio,
                                                                       Pageable pageable);

    @Query(value = """
          select case when count(d) > 0 then false else true end
          from Director d join d.films f
                        join FilmRentInfo fri on f.id = fri.film.id
          where d.id = :directorId
          and fri.returned = false
          """)
    boolean checkDirectorForDeletion(final Long directorId);
}
