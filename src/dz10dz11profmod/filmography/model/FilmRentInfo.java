package com.filmography.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Table(name = "film_rent_info")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "film_rent_info_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class FilmRentInfo
        extends GenericModel {

    @Column(name = "rent_date", nullable = false)
    private LocalDateTime rentDate;
    @Column(name = "return_date", nullable = false)
    private LocalDateTime returnDate;

    @Column(name = "returned", nullable = false)
    private Boolean returned;
    @Column(name = "rent_period", nullable = false)
    private Integer rentPeriod;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "film_id", foreignKey = @ForeignKey(name = "FK_RENT_FILM_INFO_FILM"))
    private Film film;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_RENT_FILM_INFO_USER"))
    private User user;
}

