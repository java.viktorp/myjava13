package com.filmography.mapper;

import com.filmography.dto.DirectorDTO;
import com.filmography.model.Director;
import com.filmography.model.GenericModel;
import com.filmography.repository.FilmRepository;
import com.filmography.utils.DateFormatter;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component

public class DirectorMapper
        extends GenericMapper<Director, DirectorDTO> {
    private final FilmRepository filmRepository;

    protected DirectorMapper(ModelMapper modelMapper,
                             FilmRepository filmRepository) {
        super(modelMapper, Director.class, DirectorDTO.class);
        this.filmRepository = filmRepository;
    }
    @PostConstruct
    protected void setupMapper(){
        modelMapper.createTypeMap(Director.class, DirectorDTO.class)
                .addMappings(m ->m.skip(DirectorDTO::setFilmsIds)).setPostConverter(toDTOConverter());

        modelMapper.createTypeMap(DirectorDTO.class,Director.class)
                .addMappings(m ->m.skip(Director::setFilms)).setPostConverter(toEntityconverter())
            .addMappings(m -> m.skip(Director::setBirthDate)).setPostConverter(toEntityconverter());

    }
    @Override
    protected void mapSpecificFields(DirectorDTO source, Director destination) {
        //если список режиссеров не пустой и сурс реж >0, получаем в репе из дто объекты реж. и сетим
        if(!Objects.isNull(source.getFilmsIds())&& source.getFilmsIds().size()>0){
            destination.setFilms(new HashSet<>(filmRepository.findAllById(source.getFilmsIds())));
        }else{
            //иначе пустота
            destination.setFilms(Collections.emptySet());
        }
        destination.setBirthDate(DateFormatter.formatStringToDate(source.getBirthDate()));

    }

    @Override
    protected void mapSpecificFields(Director source, DirectorDTO destination) {
        destination.setFilmsIds(getIds(source));
    }
    protected Set<Long> getIds(Director director)  {
        //что то пусто, то нул
        return Objects.isNull(director)||Objects.isNull(director.getFilms())
                ? null
                :director.getFilms().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}
