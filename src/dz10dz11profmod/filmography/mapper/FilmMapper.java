package com.filmography.mapper;

import com.filmography.dto.FilmDTO;
import com.filmography.model.Film;
import com.filmography.model.GenericModel;
import com.filmography.repository.DirectorRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class FilmMapper extends GenericMapper<Film, FilmDTO> {
    private final DirectorRepository directorRepository;

    protected FilmMapper(ModelMapper mapper,DirectorRepository directorRepository) {
        //вмето е и д подставляем фильм и дтофильм
        super(mapper, Film.class, FilmDTO.class);
        this.directorRepository=directorRepository;
    }
    @PostConstruct
    protected void setupMapper(){
        modelMapper.createTypeMap(Film.class,FilmDTO.class)
                .addMappings(m ->m.skip(FilmDTO::setDirectorsIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(FilmDTO.class,Film.class)
                .addMappings(m ->m.skip(Film::setDirectors)).setPostConverter(toEntityconverter());

    }

    @Override
    protected void mapSpecificFields(FilmDTO source, Film destination) {
        if(!Objects.isNull(source.getDirectorsIds())&& source.getDirectorsIds().size()>0){
            destination.setDirectors(new HashSet<>(directorRepository.findAllById(source.getDirectorsIds())));
        }else{
            //иначе пустота
            destination.setDirectors(Collections.emptySet());
        }

    }

    @Override
    protected void mapSpecificFields(Film source, FilmDTO destination) {
        destination.setDirectorsIds(getIds(source));
    }
    protected Set<Long> getIds(Film film)  {
        //что то пусто, то нул
        return Objects.isNull(film)||Objects.isNull(film.getDirectors())
                ? null
                :film.getDirectors().stream()
                .map(GenericModel::getId)
                .collect(Collectors.toSet());
    }
}

