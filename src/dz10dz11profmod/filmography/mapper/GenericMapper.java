package com.filmography.mapper;

import com.filmography.dto.GenericDTO;
import com.filmography.model.GenericModel;
import jakarta.annotation.PostConstruct;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Component
public abstract class GenericMapper <E extends GenericModel,D extends GenericDTO>
        implements Mapper<E,D> {
    protected final ModelMapper modelMapper;
    //класс сущности
    private final Class<E> entitiesClass;
    private final Class<D> dtoClass;

    protected GenericMapper(ModelMapper mapper,Class<E> entitiesClass,Class<D> dtoClass){
        this.modelMapper=mapper;
        this.entitiesClass=entitiesClass;
        this.dtoClass=dtoClass;
    }
    @Override
    public E toEntity(D dto){
        //статический класс, проверка на пустоту
        return Objects.isNull(dto)
                ? null
                :modelMapper.map(dto,entitiesClass);
    }
    @Override
    public List <E> toEntities(List<D> dtos){
        return dtos.stream().map(this::toEntity).toList();
    }
    @Override
    public D toDTO(E entity){
        return Objects.isNull(entity)
                ? null
                :modelMapper.map(entity,dtoClass);
    }
    @Override
    public List <D> toDTOs(List<E> entities){
        return entities.stream().map(this::toDTO).toList();
    }
    Converter<D, E> toEntityconverter(){
        return context ->{
            D source= context.getSource();
            E destination= context.getDestination();
            mapSpecificFields(source,destination);
                return context.getDestination();
        };
    }
    Converter<E,D> toDTOConverter(){
        return context ->{
            E source= context.getSource();
            D destination= context.getDestination();
            mapSpecificFields(source,destination);
                return context.getDestination();
            };
    }
    protected abstract void   mapSpecificFields(D source,E destination);
    protected abstract void   mapSpecificFields(E source,D destination);
    protected abstract Set<Long> getIds(E entity);
    @PostConstruct
    protected abstract void setupMapper();

}
