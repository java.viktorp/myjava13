package com.filmography.mapper;

import com.filmography.dto.FilmRentInfoDTO;
import com.filmography.model.FilmRentInfo;
import com.filmography.repository.FilmRepository;
import com.filmography.repository.UserRepository;
import com.filmography.servise.FilmService;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component

public class FilmRentInfoMapper extends GenericMapper<FilmRentInfo, FilmRentInfoDTO> {
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    private final FilmService filmService;
    protected FilmRentInfoMapper(ModelMapper mapper,
                                 UserRepository userRepository,
                                 FilmRepository filmRepository,
                                 FilmService filmService) {
        super(mapper, FilmRentInfo.class, FilmRentInfoDTO.class);
        this.userRepository=userRepository;
        this.filmRepository=filmRepository;
        this.filmService = filmService;
    }
    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(FilmRentInfo.class, FilmRentInfoDTO.class)
                .addMappings(m -> m.skip(FilmRentInfoDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(FilmRentInfoDTO::setFilmId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(FilmRentInfoDTO::setFilmDTO)).setPostConverter(toDTOConverter());

        super.modelMapper.createTypeMap(FilmRentInfoDTO.class, FilmRentInfo.class)
                .addMappings(m -> m.skip(FilmRentInfo::setUser)).setPostConverter(toEntityconverter())
                .addMappings(m -> m.skip(FilmRentInfo::setFilm)).setPostConverter(toEntityconverter());


    }
    @Override
    protected void mapSpecificFields(FilmRentInfoDTO source, FilmRentInfo destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильма не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(FilmRentInfo source, FilmRentInfoDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
        destination.setFilmDTO(filmService.getOne(source.getFilm().getId()));
    }

    @Override
    protected Set<Long> getIds(FilmRentInfo entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
}