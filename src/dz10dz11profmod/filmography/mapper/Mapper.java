package com.filmography.mapper;

import com.filmography.dto.GenericDTO;
import com.filmography.model.GenericModel;

import java.util.List;

//типизируемый интерфейс принимающий модель и дто
public interface Mapper<E extends GenericModel,
        D extends GenericDTO> {
    E toEntity(D dto);

    D toDTO(E entity);

    List<E> toEntities(List<D> dtosList);

    List<D> toDTOs(List<E> entitiesList);
}
