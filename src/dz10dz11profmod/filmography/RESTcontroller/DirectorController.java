package com.filmography.RESTcontroller;

import com.filmography.dto.DirectorDTO;
import com.filmography.model.Director;
import com.filmography.servise.DirectorService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами фильмотеки")
@SecurityRequirement(name = "Bearer Authentication")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DirectorController
        extends GenericController<Director, DirectorDTO> {

    private final DirectorService directorService;
    public DirectorController(DirectorService directorService) {
        super(directorService);
        this.directorService=directorService;

    }

    //находим фильм, находим режиссера и к нему добавляем фильм
//    @Operation(description = "Добавить фильм к режиссеру", method = "addFilm")
//    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Director> addFilm(@RequestParam(value = "filmId") Long filmId,
//                                            @RequestParam(value = "directorId") Long directorId) {
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильма не существует по переданному id не существует" + filmId));
//        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссера по переданному id не существует" + directorId));
//        director.getFilms().add(film);
//        return ResponseEntity.status(HttpStatus.CREATED).body(directorRepository.save(director));
//    }
}