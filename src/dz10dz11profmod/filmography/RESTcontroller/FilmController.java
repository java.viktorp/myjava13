package com.filmography.RESTcontroller;

import com.filmography.dto.FilmDTO;
import com.filmography.model.Film;
import com.filmography.servise.FilmService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
@RestController
//путь запроса, который будет слушать контроллер
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмотекой")
public class FilmController
        extends GenericController<Film, FilmDTO> {
    public FilmController( FilmService filmService) {
        super(filmService);
    }

//    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
//    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Film> addDirector(@RequestParam(value = "filmId") Long filmId,
//                                            @RequestParam(value = "directorId") Long directorId) {
//        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Режиссера по переданному id не существует" + filmId));
//        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Фильма по переданному id не существует" + directorId));
//        film.getDirectors().add(director);
//        return ResponseEntity.status(HttpStatus.CREATED).body(filmRepository.save(film));
//
//    }
}
