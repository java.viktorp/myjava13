//package com.filmography.controller;
//
//import com.filmography.config.jwt.JWTTokenUtil;
//import com.filmography.dto.LoginDTO;
//import com.filmography.dto.UserDTO;
//import com.filmography.model.User;
//import com.filmography.servise.UserService;
//import com.filmography.servise.userdetails.CustomUserDetailsService;
//import io.swagger.v3.oas.annotations.tags.Tag;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//import lombok.extern.slf4j.Slf4j;
//import java.util.HashMap;
//import java.util.Map;
//
//@Slf4j
//@RestController
//@RequestMapping("/users")
//@Tag(name = "Пользователи", description = "Контроллер по управлению пользователями")
//public class UserController
//        extends GenericController<User, UserDTO> {
//    private final CustomUserDetailsService customUserDetailsService;
//    private final JWTTokenUtil jwtTokenUtil;
//    private final UserService userService;
//
//    public UserController(UserService userService,
//                          CustomUserDetailsService customUserDetailsService,
//                          JWTTokenUtil jwtTokenUtil) {
//        super(userService);
//        this.customUserDetailsService = customUserDetailsService;
//        this.jwtTokenUtil = jwtTokenUtil;
//        this.userService = userService;
//    }
//
//    @PostMapping("/auth")
//    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
//        Map<String, Object> response = new HashMap<>();
//        log.info("LoginDTO: {}", loginDTO);
//        UserDetails foundUser = customUserDetailsService.loadUserByUsername(loginDTO.getLogin());
//        log.info("foundUser, {}", foundUser);
//        if (!userService.checkPassword(loginDTO.getPassword(), foundUser)) {
//            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Ошибка авторизации!\nНеверный пароль");
//        }
//        String token = jwtTokenUtil.generateToken(foundUser);
//        response.put("token", token);
//        response.put("username", foundUser.getUsername());
//        response.put("authorities", foundUser.getAuthorities());
//        return ResponseEntity.ok().body(response);
//    }
//}
