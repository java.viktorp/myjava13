package com.filmography.RESTcontroller;

import com.filmography.dto.FilmRentInfoDTO;
import com.filmography.model.FilmRentInfo;
import com.filmography.servise.FilmRentInfoService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/rent/info")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class RentFilmController
        extends GenericController<FilmRentInfo, FilmRentInfoDTO> {
    public RentFilmController(FilmRentInfoService filmRentInfoService) {
        super(filmRentInfoService);
    }
}
