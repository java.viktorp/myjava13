package com.filmography.servise;

import com.filmography.dto.GenericDTO;
import com.filmography.exception.MyDeleteException;
import com.filmography.mapper.GenericMapper;
import com.filmography.model.GenericModel;
import com.filmography.repository.GenericRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;


@Service
public abstract class GenericService <T extends GenericModel,N extends GenericDTO> {
    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")

    protected GenericService(GenericRepository<T> repository,
                               GenericMapper<T,N> mapper){
        this.repository=repository;
        this.mapper=mapper;
    }
    public List<N> listAll() {
        return mapper.toDTOs(repository.findAll());
    }

    public Page<N> listAll(Pageable pageable) {
        Page<T> objects = repository.findAll(pageable);
        List<N> result = mapper.toDTOs(objects.getContent());
        return new PageImpl<>(result, pageable, objects.getTotalElements());
    }
    public N getOne(Long id) {
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " не найдены")));
    }
//    public N create(N newObject){
//        //TODO: переделать, когда будет Spring Security
//        newObject.setCreatedBy("ADMIN");
//        newObject.setCreatedWhen(LocalDateTime.now());
//        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
//    }
public N create(N object) {
    object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    object.setCreatedWhen(LocalDateTime.now());
    return mapper.toDTO(repository.save(mapper.toEntity(object)));
}
    public N update(N updatedObject){
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }
    public void delete(Long id) throws MyDeleteException {
        repository.deleteById(id);
    }
    public void markAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(true);
        genericModel.setDeletedWhen(LocalDateTime.now());
        genericModel.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
    }

    public void unMarkAsDeleted(GenericModel genericModel) {
        genericModel.setDeleted(false);
        genericModel.setDeletedWhen(null);
        genericModel.setDeletedBy(null);
    }

}
