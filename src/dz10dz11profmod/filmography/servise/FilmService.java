package com.filmography.servise;

import com.filmography.constants.Errors;
import com.filmography.dto.FilmDTO;
import com.filmography.dto.FilmSearchDTO;
import com.filmography.dto.FilmWithDirectorsDTO;
import com.filmography.exception.MyDeleteException;
import com.filmography.mapper.FilmMapper;
import com.filmography.mapper.FilmWithDirectorsMapper;
import com.filmography.model.Film;
import com.filmography.repository.FilmRepository;
import com.filmography.utils.FileHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class FilmService extends GenericService<Film,FilmDTO>{

    private final FilmRepository filmRepository;
//    private final FilmMapper filmMapper;
private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository filmRepository,
                          FilmMapper filmMapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper){
        super(filmRepository,filmMapper);
        this.filmRepository=filmRepository;
        this.filmWithDirectorsMapper=filmWithDirectorsMapper;
//        this.filmMapper=filmMapper;
    }

//    public FilmDTO getOne(Long id){
//        //находим, преобразуем ентити в дто, возвращаем
//        return filmMapper.toDTO(filmRepository.findById(id).orElseThrow());
//        //получили модель
//        Film film=filmRepository.findById(id).orElseThrow(()->new NotFoundException(
//                "Данных по переданному id не существует" + id));
//        return new FilmDTO(film);
//    }

    public Page<FilmWithDirectorsDTO> getAllFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = repository.findAll(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }
    public Page<FilmWithDirectorsDTO> getAllNotDeletedFilmsWithDirectors(Pageable pageable) {
        Page<Film> filmsPaginated = filmRepository.findAllByIsDeletedFalse(pageable);
        List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
        return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
    }

    public FilmWithDirectorsDTO getFilmWithDirectors(Long id) {
        return filmWithDirectorsMapper.toDTO(mapper.toEntity(super.getOne(id)));
    }
public Page<FilmWithDirectorsDTO> findFilms(FilmSearchDTO filmSearchDTO,
                                            Pageable pageable) {
    String genre = filmSearchDTO.getGenre() != null ? String.valueOf(filmSearchDTO.getGenre().ordinal()) : null;
    Page<Film> filmsPaginated = filmRepository.searchFilms(genre,
            filmSearchDTO.getFilmTitle(),
            filmSearchDTO.getFio(),
            pageable
    );
    List<FilmWithDirectorsDTO> result = filmWithDirectorsMapper.toDTOs(filmsPaginated.getContent());
    return new PageImpl<>(result, pageable, filmsPaginated.getTotalElements());
}
    public FilmDTO create(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        object.setCreatedBy(SecurityContextHolder.getContext().getAuthentication().getName());
        object.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
    public FilmDTO update(final FilmDTO object,
                          MultipartFile file) {
        String fileName = FileHelper.createFile(file);
        object.setOnlineCopyPath(fileName);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }
    @Override
    public void delete(Long id) throws MyDeleteException {
        Film film = repository.findById(id).orElseThrow(
                () -> new NotFoundException("Фильмы с заданным ID=" + id + "     не существует"));
//        boolean filmCanBeDeleted = filmRepository.findFilmByIdAndFilmRentInfosReturnedFalseAndIsDeletedFalse(id) == null;
        boolean filmCanBeDeleted = filmRepository.checkFilmForDeletion(id);
        if (filmCanBeDeleted) {
            if (film.getOnlineCopyPath() != null && !film.getOnlineCopyPath().isEmpty()) {
                FileHelper.deleteFile(film.getOnlineCopyPath());
            }
            film.setDeleted(true);
            film.setDeletedWhen(LocalDateTime.now());
            film.setDeletedBy(SecurityContextHolder.getContext().getAuthentication().getName());
            repository.save(film);
        }
        else {
            throw new MyDeleteException(Errors.Films.FILM_DELETE_ERROR);
        }
    }
    public void restore(Long objectId) {
        Film film = repository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Фильма с заданным ID=" + objectId + " не существует"));
        unMarkAsDeleted(film);
        repository.save(film);
    }
}
