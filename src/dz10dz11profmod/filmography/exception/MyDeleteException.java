package com.filmography.exception;

public class MyDeleteException
        extends Exception {
    public MyDeleteException(String message) {
        super(message);
    }
}
