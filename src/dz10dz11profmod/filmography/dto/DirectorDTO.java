package com.filmography.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectorDTO
        extends GenericDTO {
    private String fio;
    private Integer position;
    private String birthDate;
    private String description;
    private Set<Long> filmsIds;
    private boolean isDeleted;

}
