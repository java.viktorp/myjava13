package com.filmography.dto;

import lombok.*;

import java.time.LocalDateTime;
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmRentInfoDTO extends GenericDTO {
    //    private UserDTO user;
    private LocalDateTime rentDate;
    private LocalDateTime returnDate;
    private Boolean returned;
    private Integer rentPeriod;
    private Long filmId;
    private Long userId;
    private FilmDTO filmDTO;
}
