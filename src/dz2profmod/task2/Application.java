package dz2profmod.task2;

/*
С консоли на вход подается две строки s и t. Необходимо вывести true, если
одна строка является валидной анаграммой другой строки и false иначе.
Анаграмма — это слово или фраза, образованная путем перестановки букв
другого слова или фразы, обычно с использованием всех исходных букв ровно
один раз.

 */


import java.util.*;

public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s=scanner.nextLine();
        String[] wordsS=s.split(" ");
        List<String> list= Arrays.asList(wordsS);

        String t=scanner.nextLine();
        String[] wordsT=t.split(" ");
        List<String> antoherList= Arrays.asList(wordsT);

        Collections.sort(list);
        Collections.sort(antoherList);

        System.out.println(list);
        System.out.println(antoherList);

        boolean result =list.equals(antoherList);
        System.out.println("result = "+result);

    }
}
