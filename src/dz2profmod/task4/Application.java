package dz2profmod.task4;
import java.util.*;
/*
4. В некоторой организации хранятся документы (см. класс Document). Сейчас все
документы лежат в ArrayList, из-за чего поиск по id документа выполняется
неэффективно. Для оптимизации поиска по id, необходимо помочь сотрудникам
перевести хранение документов из ArrayList в HashMap.
public class Document {
public int id;
public String name;
public int pageCount;
}
Реализовать метод со следующей сигнатурой:
public Map<Integer, Document> organizeDocuments(List<Document> documents)
 */
public class Application  {

    public static void main(String[] args) {

        Document doc1=new Document(1,"Преступление и наказание",136);
        Document doc2=new Document(2,"Война и мир",443);
        Document doc3=new Document(3,"Мёртвые души",58);
        Document doc4=new Document(4,"Отцы и дети",115);
                List<Document> list=new ArrayList<>();
        list.add(doc1);
        list.add(doc2);
        list.add(doc3);
        list.add(doc4);

        Map<Integer,Document> map=organizeDocuments(list);
        System.out.println(map.toString());
        System.out.println(map.get(3));
    }
    public static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        HashMap<Integer, Document> hashmap = new HashMap<>();
        for (Document elm : documents) {
            hashmap.put(elm.getId(),elm);
        }
        return hashmap;
    }
}


