package dz2profmod.task1;

import java.util.*;


/*
Реализовать метод, который на вход принимает ArrayList<T>, а возвращает
набор уникальных элементов этого массива. Решить используя коллекции.
 */
public class Application {
    public static void main(String[] args) {
        List<Integer> list2 = new ArrayList<>();
        list2.add(45);
        list2.add(40);
        list2.add(49);
        list2.add(48);
        list2.add(49);
        System.out.println(convertedList(list2));
    }
    public static <T > Set < T > convertedList(List < T > list) {
            Set<T> set = new HashSet<>();
            for (T elm : list)
                set.add(elm);
            return set;
        }
    }

