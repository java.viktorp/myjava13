package dz2profmod.task3;

/*
Реализовать класс PowerfulSet, в котором должны быть следующие методы:
a. public <T> Set<T> intersection(Set<T> set1, Set<T> set2) — возвращает
пересечение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {1, 2}
b. public <T> Set<T> union(Set<T> set1, Set<T> set2) — возвращает
объединение двух наборов. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}.
Вернуть {0, 1, 2, 3, 4}
c. public <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) —
возвращает элементы первого набора без тех, которые находятся также
и во втором наборе. Пример: set1 = {1, 2, 3}, set2 = {0, 1, 2, 4}. Вернуть {3}
 */

import java.util.HashSet;
import java.util.Set;

public class Application extends PowerfulSet{
    public static void main(String[] args) {
        PowerfulSet powerfulSet=new PowerfulSet();

        Set<Integer> set1 = new HashSet<>();
                set1.add(1);
                set1.add(2);
                set1.add(3);
        Set<Integer> set2=new HashSet<>();
                set2.add(0);
                set2.add(1);
                set2.add(2);
                set2.add(4);

        System.out.println(powerfulSet.intresection(set1,set2));
       set1.add(3);
        System.out.println(powerfulSet.union(set1,set2));
        set1.remove(0);
        set1.remove(4);
        System.out.println(powerfulSet.relativeComplement(set1,set2));
    }
}