package com.filmography;

import com.filmography.dto.FilmRentInfoDTO;
import com.filmography.model.FilmRentInfo;

import java.time.LocalDateTime;
import java.util.List;

public interface FilmRentInfoTestData {
    
    FilmRentInfoDTO FILM_RENT_INFO_DTO = new FilmRentInfoDTO(LocalDateTime.now(),
                                                             LocalDateTime.now(),
                                                             false,
                                                             14,
                                                             1L,
                                                             1L,
                                                             null);
    
    List<FilmRentInfoDTO> FILM_RENT_INFO_DTO_LIST = List.of(FILM_RENT_INFO_DTO);
    
    FilmRentInfo FILM_RENT_INFO = new FilmRentInfo(
                                                   LocalDateTime.now(),
                                                   LocalDateTime.now(),
                                                   false,
                                                   14,
                                                   null,
                                                   null);
    
    List<FilmRentInfo> FILM_RENT_INFO_LIST = List.of(FILM_RENT_INFO);
}
