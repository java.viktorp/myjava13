package com.filmography.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.filmography.dto.AddFilmDTO;
import com.filmography.dto.DirectorDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
public class DirectorRestControllerTest
      extends CommonTestREST {

    private static Long createdDirectorID;

    @Test
    @Order(0)
    protected void listAll() throws Exception {
        log.info("Тест по просмотра всех режиссеров через REST начат успешно");
        String result = mvc.perform(
                    get("/rest/directors/getAll")
                          .headers(headers)
                          .contentType(MediaType.APPLICATION_JSON)
                          .accept(MediaType.APPLICATION_JSON))
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andExpect(jsonPath("$.*", hasSize(greaterThan(0))))
              .andReturn()
              .getResponse()
              .getContentAsString();
        List<DirectorDTO> authorDTOS = objectMapper.readValue(result, new TypeReference<List<DirectorDTO>>() {});
        authorDTOS.forEach(a -> log.info(a.toString()));
        log.info("Тест по просмотра всех директоров через REST закончен успешно");
    }

    @Test
    @Order(1)
    protected void createObject() throws Exception {
        log.info("Тест по созданию директора через REST начат успешно");
        //Создаем нового директора для создания через контроллер (тест дата)
        DirectorDTO authorDTO = new DirectorDTO("REST_TestDirectorFio", "2023-01-01", "Test Description", new HashSet<>(), false,4);


        DirectorDTO result = objectMapper.readValue(mvc.perform(post("/rest/directors/add")
                                                                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                    .headers(super.headers)
                                                                    .content(asJsonString(authorDTO))
                                                                    .accept(MediaType.APPLICATION_JSON_VALUE))
                                                        .andExpect(status().is2xxSuccessful())
                                                        .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                                                        .andReturn()
                                                        .getResponse()
                                                        .getContentAsString(),
                                                  DirectorDTO.class);
        createdDirectorID = result.getId();
        log.info("Тест по созданию директора через REST закончен успешно " + result);

    }

    @Test
    @Order(2)
    protected void updateObject() throws Exception {
        log.info("Тест по обновления директора через REST начат успешно");
        //получаем нашего директора созданного (если запускать тесты подряд), если отдельно - создаем отдельную тест дату для апдейта
        DirectorDTO existingDirector = objectMapper.readValue(mvc.perform(get("/rest/directors/getOneById")
                                                                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                            .headers(super.headers)
                                                                            .param("id", String.valueOf(createdDirectorID))
                                                                            .accept(MediaType.APPLICATION_JSON_VALUE))
                                                                .andExpect(status().is2xxSuccessful())
                                                                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                                                                .andReturn()
                                                                .getResponse()
                                                                .getContentAsString(),
                                                          DirectorDTO.class);
        //обновляем поля
        existingDirector.setFio("REST_TestDirectorFioUPDATED");
        existingDirector.setDescription("Test Description UPDATED");

        //вызываем update через REST API
        mvc.perform(put("/rest/directors/update")
                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                          .headers(super.headers)
                          .content(asJsonString(existingDirector))
                          .param("id", String.valueOf(createdDirectorID))
                          .accept(MediaType.APPLICATION_JSON_VALUE))
              .andDo(print())
              .andExpect(status().is2xxSuccessful());
        log.info("Тест по обновления директора через REST закончен успешно");
    }

    @Test
    @Order(3)
    void addFilm() throws Exception {
        log.info("Тест по добавлению книги директору через REST начат успешно");
        AddFilmDTO addFilmDTO = new AddFilmDTO(11L, createdDirectorID);
        String result = mvc.perform(post("/rest/directors/addFilm")
                                          .contentType(MediaType.APPLICATION_JSON_VALUE)
                                          .headers(super.headers)
                                          .content(asJsonString(addFilmDTO))
                                          .accept(MediaType.APPLICATION_JSON_VALUE))
              .andDo(print())
              .andExpect(status().is2xxSuccessful())
              .andReturn()
              .getResponse()
              .getContentAsString();

        DirectorDTO author = objectMapper.readValue(result, DirectorDTO.class);
        log.info("Тест по добавлению книги директору через REST завершен успешно с результатом {}",
                 author);
    }

    @Test
    @Order(4)
    protected void deleteObject() throws Exception {
        log.info("Тест по удалению директора через REST начат успешно");
        mvc.perform(delete("/rest/directors/delete/{id}", createdDirectorID)
                          .headers(headers)
                          .contentType(MediaType.APPLICATION_JSON)
                          .accept(MediaType.APPLICATION_JSON)
                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful());
        DirectorDTO existingDirector = objectMapper.readValue(mvc.perform(get("/rest/directors/getOneById")
                                                                            .contentType(MediaType.APPLICATION_JSON_VALUE)
                                                                            .headers(super.headers)
                                                                            .param("id", String.valueOf(createdDirectorID))
                                                                            .accept(MediaType.APPLICATION_JSON_VALUE))
                                                                .andExpect(status().is2xxSuccessful())
                                                                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                                                                .andReturn()
                                                                .getResponse()
                                                                .getContentAsString(),
                                                          DirectorDTO.class);

        assertTrue(existingDirector.isDeleted());
        log.info("Тест по удалению директора через REST завершен успешно");
        mvc.perform(
                    delete("/rest/directors/delete/hard/{id}", createdDirectorID)
                          .headers(headers)
                          .contentType(MediaType.APPLICATION_JSON)
                          .accept(MediaType.APPLICATION_JSON)
                   )
              .andDo(print())
              .andExpect(status().is2xxSuccessful());
        log.info("Данные очищены");
    }

}
