package com.filmography.filmography.service;


import com.filmography.dto.FilmDTO;
import com.filmography.exception.MyDeleteException;
import com.filmography.model.Film;
import org.junit.jupiter.api.Test;

public class FilmServiceTest extends GenericTest<Film, FilmDTO> {
    
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Test
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
