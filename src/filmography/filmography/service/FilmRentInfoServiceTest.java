package com.filmography.filmography.service;


import com.filmography.dto.FilmRentInfoDTO;
import com.filmography.exception.MyDeleteException;
import com.filmography.model.FilmRentInfo;
import org.junit.jupiter.api.Test;

public class FilmRentInfoServiceTest extends GenericTest<FilmRentInfo, FilmRentInfoDTO> {
    @Test
    @Override
    protected void getAll() {
    
    }
    
    @Test
    @Override
    protected void getOne() {
    
    }
    
    @Test
    @Override
    protected void create() {
    
    }
    
    @Test
    @Override
    protected void update() {
    
    }
    
    @Test
    @Override
    protected void delete() throws MyDeleteException {
    
    }
    
    @Override
    protected void restore() {
    
    }
    
    @Test
    @Override
    protected void getAllNotDeleted() {
    
    }
}
