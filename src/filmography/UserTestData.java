package com.filmography;


import com.filmography.dto.RoleDTO;
import com.filmography.dto.UserDTO;
import com.filmography.model.Role;
import com.filmography.model.User;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

public interface UserTestData {
    
    UserDTO USER_DTO = new UserDTO("login",
                                   "password",
            "firstName",
            "lastName",
            "middleName",
            "birthDate",
            "phone",
            "address",
            "email",
            new RoleDTO(),
            new HashSet<>(),
            "changePasswordToken",
            false
    );
    
    List<UserDTO> USER_DTO_LIST = List.of(USER_DTO);
    
    User USER = new User("login",
                         "password",
            "firstName",
            "lastName",
            "middleName",
            LocalDate.now(),
                         "phone",
                         "address",
            "email",
            "changePasswordToken",
            new Role(),
            new HashSet<>(),
            new HashSet<>()
    );
    
    List<User> USER_LIST = List.of(USER);
}
