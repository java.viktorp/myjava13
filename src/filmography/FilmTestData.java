package com.filmography;


import com.filmography.dto.DirectorDTO;
import com.filmography.dto.FilmDTO;
import com.filmography.dto.FilmWithDirectorsDTO;
import com.filmography.model.Film;
import com.filmography.model.Genre;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface FilmTestData {
    FilmDTO FILM_DTO_1 = new FilmDTO("bookTitle1",
            2023,
            "country1",
            Genre.DRAMA,
            24,
            "description1",
            "onlineCopyPath1",
            new HashSet<>(),
            false);

    FilmDTO FILM_DTO_2 = new FilmDTO("bookTitle2",
            2022,
            "country2",
            Genre.DRAMA,
            12,
            "description2",
            "onlineCopyPath2",
            new HashSet<>(),
            false);

    List<FilmDTO> FILM_DTO_LIST = Arrays.asList(FILM_DTO_1, FILM_DTO_2);

    Film FILM_1 = new Film("bookTitle1",
            2023,
            "country1",
            Genre.DRAMA,
            24,
            "description1",
            "onlineCopyPath1",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>());
    Film FILM_2 = new Film("bookTitle2",
            2022,
            "country2",
            Genre.DRAMA,
            24,
            "description2",
            "onlineCopyPath2",
            new HashSet<>(),
            new HashSet<>(),
            new HashSet<>());

    List<Film> FILM_LIST = Arrays.asList(FILM_1, FILM_2);

    Set<DirectorDTO> DIRECTORS = new HashSet<>(DirectorTestData.DIRECTOR_DTO_LIST);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_1 = new FilmWithDirectorsDTO(FILM_1, DIRECTORS);
    FilmWithDirectorsDTO FILM_WITH_DIRECTORS_DTO_2 = new FilmWithDirectorsDTO(FILM_2, DIRECTORS);

    List<FilmWithDirectorsDTO> FILM_WITH_DIRECTORS_DTO_LIST = Arrays.asList(FILM_WITH_DIRECTORS_DTO_1, FILM_WITH_DIRECTORS_DTO_2);
}
