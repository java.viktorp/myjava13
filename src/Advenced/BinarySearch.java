package Advenced;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinarySearch {
    public static void main(String[] args) {
        List<Integer> list=new ArrayList<>();
        for (int i = 0; i < 10; i++) {
           list.add(i);
        }
        //Collections.sort(list); обязательная сортировка
        int index = Collections.binarySearch(list,5);
        System.out.println(index);
        System.out.println(list.get(index));
    }
}
