package Advenced;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Field;

public class Application {
    public static void main(String[] args) {
        Application application= new Application();
        Person person = new Person();
        Application.print(person,person.getClass());

    }

    static void print(Object o, Class c) {
        Field[] fields = c.getDeclaredFields();
        for (Field field : fields) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation annotation : annotations) {
                if (annotation.annotationType().equals(Show.class)) {
                    try {
                        System.out.println(field.get(o));
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }
}
@Retention(RetentionPolicy.RUNTIME)
@interface Show {
    boolean value() default true;
}
class Person {
    String name = "Ivan";
    @Show
    String othername = "Kiril";
}
