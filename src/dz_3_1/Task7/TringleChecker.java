package dz_3_1.Task7;

public class TringleChecker {

    public static boolean isValidTriangle(double a, double b, double c) {
        return (a + b) > c && (a + c) > b && (b + c) > a;
    }
    /*
    public class TestTriangleChecker {
    public static void main(String[] args) {

        TringleChecker tringlechecker = new TringleChecker();

        System.out.println(TringleChecker.isValidTriangle(0.9,2.2,1.6));
        System.out.println(TringleChecker.isValidTriangle(12.2,25,9));

    }
}
     */
}
