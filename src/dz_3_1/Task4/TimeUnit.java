package dz_3_1.Task4;

public class TimeUnit {
    private int seconds;
    private int minutes;
    private int hours;


    public TimeUnit(int hours, int minutes, int seconds) {
        this.seconds = seconds;
        this.minutes = minutes;
        this.hours = hours;
    }

    public TimeUnit(int hours,int minutes) {
        this.minutes = minutes;
        this.hours = hours;
        this.seconds = 0;
    }

    public TimeUnit(int hours) {
        this.hours = hours;
        this.minutes = 0;
        this.seconds = 0;
    }

    public void printTimeUnit() {
        String hours1 = String.valueOf(hours);
        if (hours1.length() < 2) {
            hours1 = "0" + hours1;
        }
        String minutes1 = String.valueOf(minutes);
        if (minutes1.length() < 2) {
            minutes1 = "0" + minutes1;
        }
        String seconds1 = String.valueOf(seconds);
        if (seconds1.length() < 2) {
            seconds1="0"+seconds1;
        }
        System.out.print(hours1+":"+minutes1+":"+seconds1);
    }
    public void printTimeUnit12f(){
        if(hours<12){
            printTimeUnit();
            System.out.println(" am");
        }else{
            printTimeUnit();
            System.out.println(" pm");
        }
    }
    public boolean isValidation(int hours1,int minutes1,int seconds1){
        return hours1>23||minutes1>59||seconds1>59;

    }
    public void addTime(int hours1,int minutes1,int seconds1){
        if(isValidation(hours1,minutes1,seconds1)){
            System.out.println("������� ����������� ��������");
        }else{
            hours+=hours1;
            if(hours>23){
                hours-=24;
                ++hours;
            }
            minutes+=minutes1;
            if(minutes>59){
                minutes-=60;
                ++minutes1;
            }
            seconds+=seconds1;
            if(seconds>59){
                seconds-=60;
                ++seconds;
            }
        }
    }
    /*
    public class TimeUnitTest {
    public static void main(String[] args) {
        TimeUnit timeUnit=new TimeUnit(15,20,55);
        timeUnit.printTimeUnit();
        System.out.println();
        timeUnit.printTimeUnit12f();
        System.out.println();
        timeUnit.addTime(11,12,7);
        timeUnit.printTimeUnit();
        System.out.println();
        timeUnit.printTimeUnit12f();
    }
}
     */
}

