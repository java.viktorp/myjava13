package dz_3_1.Task8;

public class Atm {
    private double roublesPerDollars;
    private double dollarsPerRoubles;
    public static int instance;

    public Atm(double roublesPerDollars,double dollarsPerRoubles) {
        this.roublesPerDollars=roublesPerDollars;
        this.dollarsPerRoubles=dollarsPerRoubles;
       instance++;
    }
    public double convertedRoublestoDollars(double  dollars){
        return (int)(dollars*roublesPerDollars*100)/100.0;
    }
    public double convertedDollarsPerRoubles(double  roubles){
        return (int)(roubles*roublesPerDollars*100)/100.0;
    }
    public static int getInstance(){
        return instance;
    }

    /*
    public class AtmTest {
    public static void main(String[] args) {
        Atm atm = new Atm(62, 0.18);
        Atm atm1 = new Atm(62.48, 0.17);
        Atm atm2 = new Atm(60, 0.22);
        Atm atm3 = new Atm(61.66, 0.18);

        System.out.println(atm.convertedDollarsPerRoubles(100));
        System.out.println(atm.convertedRoublestoDollars(100));
        System.out.println(Atm.getInstance());
    }
}
     */
}
