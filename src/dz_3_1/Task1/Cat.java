package dz_3_1.Task1;

public class Cat {
    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }

    public void status() {
        double n = (int) (Math.random() * 3);
        if (n == 0) {
            this.sleep();
        } else if (n == 1) {
            this.meow();
        } else {
            this.eat();
        }
    }
/*

    public static void main(String[] args) {
        Cat cat=new Cat();
        cat.status();
    }

*/
}
