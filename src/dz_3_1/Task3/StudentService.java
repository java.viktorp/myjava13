package dz_3_1.Task3;


import dz_3_1.Task2.Student;

public class StudentService {
    public static Student bestStudent(Student[] st) {
        Student best = st[0];
        for (int i = 1; i < st.length; i++) {
            if (best.averageMark(best.getGrades()) < st[i].averageMark(st[i].getGrades())) {
                best = st[i];
            }
        }
        return best;
    }

    public static void sortBySurname(Student[] st) {
        int compareResult;
        Student temp;
        for (int i = 0; i < st.length - 1; i++) {
            compareResult = st[i].getSurname().compareTo(st[i + 1].getSurname());
            if (compareResult > 0) {
                temp = st[i];
                st[i] = st[i + 1];
                st[i + 1] = temp;
            }
        }
    }
    /*
        public static void main(String[] args) {
            StudentService studentService=new StudentService();
            int[] grades1 = {2, 3, 2, 1, 4, 5};
            int[] grades2 = {1, 4, 2, 1, 2, 5};
            int[] grades3 = {4, 5, 2, 4, 4, 5};
            Student student1 = new Student("Илья", "Корж", grades1);
            Student student2 = new Student("Владимир", "Суворов", grades2);
            Student student3 = new Student("Кира", "Малышкина", grades3);
            Student[] students = {student1, student2, student3};

            System.out.print("Лучший студент: "+Arrays.toString(new String[]{StudentService.bestStudent(students).getName()})+" ");
            System.out.println(Arrays.toString(new String[]{StudentService.bestStudent(students).getSurname()}));
            System.out.println();
            System.out.println("Сортировка студентов по фамилии:");


            StudentService.sortBySurname(students);
            for (Student s: students) {
                System.out.println(s.getSurname()+" "+s.getName());
            }

        }

     */
    }










