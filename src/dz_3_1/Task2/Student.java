package dz_3_1.Task2;

public class Student {
    private String name;
    private String surname;
    private int[] grades;


    public Student(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Student(String name, String surname, int[] grades) {
        this.name = name;
        this.surname = surname;
        this.grades = grades;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    public int[] newGrades(int[] d, int x) {
        int tmp = d[0];
        for (int i = 1; i < d.length; i++) {
            d[i - 1] = d[i];
        }
        d[d.length - 1] = x;
        return d;
    }

    public double averageMark(int[] d) {
        int total = 0;
        for (int i = 0; i < d.length; i++) {
            total += d[i];
        }
        return (int) (total / d.length * 10) / 10.0;
    }

/*
public class testStudent {
    public static void main(String[] args) {
        Student student = new Student("Семен","Пушка");
        int[] grades = {1, 2, 2, 3, 3, 3};
        int newScore = 5;
        student.setGrades(grades);
        String str = Arrays.toString(student.newGrades(grades, newScore));
        double x = student.averageMark(student.newGrades(grades, newScore));
        System.out.println("Оценки студента "+student.getName()+" "+student.getSurname()+" " + str);
        System.out.print("Его средний балл: " + x);
        }
}

 */
}