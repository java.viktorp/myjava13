package dz_3_1.Task6;

public class AmazingString {
    private char[] chars;


    public AmazingString(char[] chars) {
        this.chars = chars;
    }

    public AmazingString(String string) {
        this.chars = string.toCharArray();
    }

    public char getChar(int i) {
        return chars[i];
    }

    public void getLength() {
        System.out.println(chars.length);
    }

    public void printChars() {
        System.out.println(chars);
    }

    public boolean isSubString(char[] sub) {
        int count = 0;
        int startSub = 0;
        for (int i = 0; i < chars.length; i++) {
            if (sub[0] == chars[i]) {
                startSub = i;
                break;
            }
        }
        for (int i = startSub, j = 0; j < sub.length; i++, j++) {
            if (chars[i] == sub[j]) {
                count++;
            } else {
                break;
            }
        }
        return count == sub.length;
    }

    public boolean isSubstring(String substr) {
        return isSubString(substr.toCharArray());
    }

    public void removeLeadSpaceChar() {
        if (chars[0] == ' ') {
            int count = 1;
            for (int i = 1; i <= count; i++) {
                if (chars[i] == ' ') {
                    count++;
                }
            }
            char[] chars1 = new char[chars.length - count];
            for (int i = count, j = 0; i < chars.length; i++, j++) {
                chars1[j] = chars[i];
            }
            chars = chars1;
        } else {
            chars = chars;
        }
    }


    public void reverse() {
        for (int i = 0, j = chars.length - 1; i < chars.length / 2; i++, j--) {
            int temp = chars[i];
            chars[i] = chars[j];
            chars[j] = chars[i];
        }
    }
    /*
    public class TestAmazingString {
    public static void main(String[] args) {
        AmazingString amazingstring=new AmazingString(new char[]{' ',' ','j','a','v','a','d','o','c'});
        System.out.println(amazingstring.getChar(5));
        amazingstring.getLength();
        amazingstring.printChars();
        System.out.println(amazingstring.isSubString(new char[]{'v','a','d'}));
        System.out.println(amazingstring.isSubstring("gad"));
        amazingstring.removeLeadSpaceChar();
        amazingstring.printChars();
        amazingstring.reverse();
        amazingstring.printChars();
    }
}
     */
}
