package dz2_1;


import java.util.Scanner;

public class Task10 {
    public static void main(String[] args) {

        game();
    }
    public static void game() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Здравствуйте. Давайте сыграем в игру.\nПопробуйте угадать число" +
                " от 0 до 1000 включительно, которое загадал компьютер.\nУ вас бесконечное количество попыток," +
                " если вы не угадаете, компьютер сообщит\nявляется ли число больше или меньше загаданного числа. " +
                "\nЧисло уже загадано, итак начнем: ");
        int m = (int) (Math.random() * 1001);
        System.out.println(m);
        for (int i = 0; ; i++) {
            int n = scanner.nextInt();
            if (n == m) {
                System.out.println("Победа!");
                break;
            } else if (n > m) {
                System.out.println("Это число больше заданного.");
            } else {
                System.out.println("Это число меньше заданного.");
            }
        }
    }
}
