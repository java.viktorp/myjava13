create table buyers(
    id serial primary key ,
    name varchar(30) not null,
    phone varchar(30) not null
);

select *
from buyers;

insert into buyers(name, phone)
values ('Viktor',5552555);
insert into buyers(name, phone)
values ('Marina',5553030);
insert into buyers(name, phone)
values ('Alex',5557070);
