create table orders_info(
    id bigserial primary key,
    --buyers_id ссылается на таблицу buyers на поле id
    buyer_id integer references buyers(id),
    sku varchar(30) not null ,
    count integer not null ,
    price_piece integer not null ,
    price integer not null,
    date_added timestamp
);
select *
from orders_info;

insert into orders_info(buyer_id, sku, count, price_piece, price, date_added)
values (2,'roses',10,100,1000,now());

insert into orders_info(buyer_id, sku, count, price_piece, price, date_added)
values (1,'lilies',15,50,750,now());

insert into orders_info(buyer_id, sku, count, price_piece, price, date_added)
values (3,'chamomile',20,25,500,now());

insert into orders_info(buyer_id, sku, count, price_piece, price, date_added)
values (1,'roses',15,100,1500,now());

drop table orders_info;


--1. По идентификатору заказа получить данные заказа и данные клиента, создавшего этот заказ
select *
from orders_info o,
     buyers b
where o.buyer_id=b.id;

select *
from orders_info o join buyers b on b.id=o.buyer_id;

--2. Получить данные всех заказов одного клиента по идентификатору клиента за последний месяц
select *
from orders_info o
where o.buyer_id=1;

--3. Найти заказ с максимальным количеством купленных цветов, вывести их название и количество
select *
from orders_info
where count in (select max(count) from orders_info);

--4. Вывести общую выручку (сумму золотых монет по всем заказам) за все время
select sum(price)
from orders_info;




