create table orders(
    id serial primary key ,
    sku varchar(30) not null ,
    count varchar(30) not null ,
    price_piece varchar(30) not null ,
    price varchar(30) not null,
    date_added timestamp
);

select *
from orders;

insert into orders(sku, count, price_piece, price, date_added)
values ('roses',10,100,1000,now());

insert into orders(sku, count, price_piece, price, date_added)
values ('lilies',15,50,750,now());

insert into orders(sku, count, price_piece, price, date_added)
values ('chamomile',20,25,500,now());



drop table orders;
