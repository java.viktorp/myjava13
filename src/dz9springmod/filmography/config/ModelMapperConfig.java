package dz9springmod.filmography.config;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.modelmapper.config.Configuration.AccessLevel.PRIVATE;

@Configuration
public class ModelMapperConfig {

    @Bean
    public ModelMapper modelMapper(){
        ModelMapper mapper=new ModelMapper();
        mapper.getConfiguration()
                //строго сверяет поля
                .setMatchingStrategy(MatchingStrategies.STRICT)
                //метчинг происходит по полям
                .setFieldMatchingEnabled(true)
                //пропускаем устые поля
                .setSkipNullEnabled(true)
                //поля у классов маппера будут приватными
                .setFieldAccessLevel(PRIVATE);
                return mapper;
    }
}
