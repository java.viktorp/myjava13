package dz9springmod.filmography.repository;

import dz9springmod.filmography.model.User;

public interface UserRepository extends GenericRepository<User> {
}
