package dz9springmod.filmography.repository;

import dz9springmod.filmography.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
}
