package dz9springmod.filmography.repository;

import dz9springmod.filmography.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Film> {
}
