package dz9springmod.filmography.repository;

import dz9springmod.filmography.model.Order;

public interface OrderRepository extends GenericRepository<Order> {
}
