package dz9springmod.filmography.mapper;

import dz9springmod.filmography.dto.OrderDTO;
import dz9springmod.filmography.model.Order;
import dz9springmod.filmography.repository.FilmRepository;
import dz9springmod.filmography.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import org.webjars.NotFoundException;

import java.util.Set;

@Component

public class OrderMapper extends GenericMapper<Order, OrderDTO> {
    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    protected OrderMapper(ModelMapper mapper,
                          UserRepository userRepository,
                          FilmRepository filmRepository) {
        super(mapper, Order.class, OrderDTO.class);
        this.userRepository=userRepository;
        this.filmRepository=filmRepository;
    }
    @PostConstruct
    public void setupMapper() {
        super.modelMapper.createTypeMap(Order.class, OrderDTO.class)
                .addMappings(m -> m.skip(OrderDTO::setUserId)).setPostConverter(toDTOConverter())
                .addMappings(m -> m.skip(OrderDTO::setFilmId)).setPostConverter(toDTOConverter());
    }
    @Override
    protected void mapSpecificFields(OrderDTO source, Order destination) {
        destination.setFilm(filmRepository.findById(source.getFilmId()).orElseThrow(() -> new NotFoundException("Фильма не найдено")));
        destination.setUser(userRepository.findById(source.getUserId()).orElseThrow(() -> new NotFoundException("Пользователя не найдено")));
    }

    @Override
    protected void mapSpecificFields(Order source, OrderDTO destination) {
        destination.setUserId(source.getUser().getId());
        destination.setFilmId(source.getFilm().getId());
    }

    @Override
    protected Set<Long> getIds(Order entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }


}