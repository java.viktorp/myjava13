package dz9springmod.filmography.mapper;

import dz9springmod.filmography.dto.UserDTO;
import dz9springmod.filmography.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserMapper extends GenericMapper<User, UserDTO> {
    public UserMapper(ModelMapper modelMapper) {
        super(modelMapper, User.class, UserDTO.class);
    }

    @Override
    protected void mapSpecificFields(UserDTO source, User destination) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

    @Override
    protected void mapSpecificFields(User source, UserDTO destination) {
        throw new UnsupportedOperationException("Метод недоступен");
    }

    @Override
    protected Set<Long> getIds(User entity) {
        throw new UnsupportedOperationException("Метод недоступен");
    }
    @Override
    protected void setupMapper() {
        modelMapper.createTypeMap(User.class, UserDTO.class)
                .addMappings(m -> m.skip(UserDTO::setOrdersIds)).setPostConverter(toDTOConverter());
        modelMapper.createTypeMap(UserDTO.class, User.class)
                .addMappings(m -> m.skip(User::setOrders)).setPostConverter(toEntityconverter());
    }

}
