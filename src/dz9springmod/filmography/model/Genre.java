package dz9springmod.filmography.model;

public enum Genre {
    DRAMA("Драма"),

    NOVEL("Роман"),
    FANTASY("Фантастика");
    private final String genreTextDisplay;
    Genre(String genreTextDisplay) {
        this.genreTextDisplay = genreTextDisplay;
    }

    public String getGenreTextDisplay() {
        return genreTextDisplay;
    }


}
