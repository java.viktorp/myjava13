package dz9springmod.filmography.controller;

import dz9springmod.filmography.dto.UserDTO;
import dz9springmod.filmography.model.User;
import dz9springmod.filmography.servise.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи", description = "Контроллер по управлению пользователями")
public class UserController
        extends GenericController<User, UserDTO> {
    public UserController(UserService userService) {
        super(userService);
    }
}
