package dz9springmod.filmography.controller;

import dz9springmod.filmography.dto.OrderDTO;
import dz9springmod.filmography.model.Order;
import dz9springmod.filmography.servise.OrderService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class OrderController
        extends GenericController<Order, OrderDTO> {
    public OrderController(OrderService orderService) {
        super(orderService);
    }
}
