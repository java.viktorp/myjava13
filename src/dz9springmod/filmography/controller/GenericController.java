package dz9springmod.filmography.controller;

import dz9springmod.filmography.dto.GenericDTO;
import dz9springmod.filmography.model.GenericModel;
import dz9springmod.filmography.servise.GenericService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController

public abstract class GenericController<T extends GenericModel,N extends GenericDTO> {

    private final GenericService<T,N> service;

//    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    public GenericController(GenericService<T,N> service) {
        this.service=service;
    }

    //http://localhost:8080/api/rest/films/getById?id=1
    @Operation(description = "Получить запись по ID", method = "getOneById")
    @GetMapping(value = "/getOneById", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> getOneById(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(service.getOne(id));
    }
    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<N>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(service.listAll());
    }


    @Operation(description = "Создать новый объект", method = "create")
    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> create(@RequestBody N newEntity) {
        //Создание в тот момент, когда запрос доходит до сервера
        newEntity.setCreatedWhen(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(newEntity));
    }

    @Operation(description = "Обновить объект", method = "update")
    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<N> update(@RequestBody N updateEntity,
                                    @RequestParam(value = "id") Long id) {
        updateEntity.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED).body(service.update(updateEntity));
    }

    @Operation(description = "Удалить объект", method = "delete")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "id") Long id) {
        service.delete(id);
    }
}
