package dz9springmod.filmography.servise;

import dz9springmod.filmography.dto.OrderDTO;
import dz9springmod.filmography.mapper.OrderMapper;
import dz9springmod.filmography.model.Order;
import dz9springmod.filmography.repository.OrderRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService extends GenericService<Order, OrderDTO> {
    protected OrderRepository orderRepository;
    protected OrderService(OrderRepository orderRepository,
                           OrderMapper orderMapper) {
        super(orderRepository, orderMapper);
        this.orderRepository = orderRepository;
    }
}
