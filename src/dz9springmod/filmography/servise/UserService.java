package dz9springmod.filmography.servise;

import dz9springmod.filmography.dto.RoleDTO;
import dz9springmod.filmography.dto.UserDTO;
import dz9springmod.filmography.mapper.UserMapper;
import dz9springmod.filmography.model.User;
import dz9springmod.filmography.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService extends GenericService<User, UserDTO> {
    protected UserService(UserRepository userRepository,
                          UserMapper userMapper) {
        super(userRepository, userMapper);
    }
    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        return mapper.toDTO(repository.save(mapper.toEntity(object)));
    }

}