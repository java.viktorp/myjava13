package dz9springmod.filmography.servise;

import dz9springmod.filmography.dto.FilmDTO;
import dz9springmod.filmography.dto.FilmWithDirectorsDTO;
import dz9springmod.filmography.mapper.FilmMapper;
import dz9springmod.filmography.mapper.FilmWithDirectorsMapper;
import dz9springmod.filmography.model.Film;
import dz9springmod.filmography.repository.FilmRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService extends GenericService<Film, FilmDTO>{

    protected FilmRepository filmRepository;
//    private final FilmMapper filmMapper;
private final FilmWithDirectorsMapper filmWithDirectorsMapper;

    protected FilmService(FilmRepository filmRepository,
                          FilmMapper filmMapper,
                          FilmWithDirectorsMapper filmWithDirectorsMapper){
        super(filmRepository,filmMapper);
        this.filmRepository=filmRepository;
        this.filmWithDirectorsMapper=filmWithDirectorsMapper;
//        this.filmMapper=filmMapper;
    }
    public List<FilmWithDirectorsDTO> getAllFilmsWithDirectors() {
        return filmWithDirectorsMapper.toDTOs(repository.findAll());
    }
//    public FilmDTO getOne(Long id){
//        //находим, преобразуем ентити в дто, возвращаем
//        return filmMapper.toDTO(filmRepository.findById(id).orElseThrow());
//        //получили модель
//        Film film=filmRepository.findById(id).orElseThrow(()->new NotFoundException(
//                "Данных по переданному id не существует" + id));
//        return new FilmDTO(film);
//    }
}
