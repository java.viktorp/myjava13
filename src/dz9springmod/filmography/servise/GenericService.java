package dz9springmod.filmography.servise;

import dz9springmod.filmography.dto.GenericDTO;
import dz9springmod.filmography.mapper.GenericMapper;
import dz9springmod.filmography.model.GenericModel;
import dz9springmod.filmography.repository.GenericRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.List;

@Service
public abstract class GenericService <T extends GenericModel,N extends GenericDTO> {
    protected final GenericRepository<T> repository;
    protected final GenericMapper<T, N> mapper;
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")

    public GenericService(GenericRepository<T> repository,
                          GenericMapper<T,N> mapper){
        this.repository=repository;
        this.mapper=mapper;
    }

    //получили список дто
    public List<N> listAll(){
        return mapper.toDTOs(repository.findAll());
    }
    public N getOne(final Long id){
        return mapper.toDTO(repository.findById(id).orElseThrow(() -> new NotFoundException("Данных по заданному id: " + id + " не найдены")));
    }
    public N create(N newObject){
        //TODO: переделать, когда будет Spring Security
        newObject.setCreatedBy("ADMIN");
        newObject.setCreatedWhen(LocalDateTime.now());
        return mapper.toDTO(repository.save(mapper.toEntity(newObject)));
    }
    public N update(N updatedObject){
        return mapper.toDTO(repository.save(mapper.toEntity(updatedObject)));
    }
    public void delete(final Long id){
        repository.deleteById(id);
    }

}
