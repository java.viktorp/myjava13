package dz9springmod.filmography.servise;

import dz9springmod.filmography.dto.DirectorDTO;
import dz9springmod.filmography.mapper.DirectorMapper;
import dz9springmod.filmography.model.Director;
import dz9springmod.filmography.repository.DirectorRepository;
import org.springframework.stereotype.Service;

@Service

public class DirectorService extends GenericService<Director, DirectorDTO> {
    protected DirectorRepository directorRepository;
    protected DirectorService(DirectorRepository directorRepository,
                              DirectorMapper directorMapper){
        super(directorRepository,directorMapper);
        this.directorRepository=directorRepository;
    }
}
