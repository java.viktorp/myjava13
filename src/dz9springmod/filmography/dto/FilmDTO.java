package dz9springmod.filmography.dto;

import dz9springmod.filmography.model.Genre;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FilmDTO extends GenericDTO{
    private String filmTitle;
    private Integer premierYear;
    private String country;
    private Genre gene;
    private Set<Long> directorsIds;
    private Set<Long> ordersIds;

//    public FilmDTO(Film film){
//        //создали дто
//        FilmDTO filmDTO=new FilmDTO();
//        //поля делаем из ентити в дто
//        filmDTO.setFilmTitle(film.getFilmTitle());
//        filmDTO.setPremierYear(film.getPremierYear());
//        filmDTO.setCountry(film.getCountry());
//        filmDTO.setGene(film.getGene());
//        Set<Director> directors =film.getDirectors();
//        //достали ид режисеров
//        Set<Long> directorsIds=new HashSet();
//        directors.forEach(a-> directorsIds.add(a.getId()));
//        //просетили ид режиссеров
//        filmDTO.setDirectorsIds(directorsIds);
//    }
//
//    public List<FilmDTO> getFilmDTOs(List<Film>films){
//        List<FilmDTO> filmDTOS=new ArrayList<>();
//        for (Film film:films){
//            filmDTOS.add(new FilmDTO(film));
//        }
//        return filmDTOS;
//    }
}
