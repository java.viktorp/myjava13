package dz1profmod.task2;

public class DaysException extends RuntimeException{
    public DaysException() {
    }

    public DaysException(String message) {
        super(message);
    }
}

