package dz1profmod.task2;

import java.util.Scanner;

public class MyUncheckedException {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String[] days = {"понедельник", "вторник", "среда", "четверг", "пятница", "суббота", "воскресенье"};
        int[] num = {1, 2, 3, 4, 5, 6, 7};
        while (true) {
            System.out.println("Наберите любую цифру от " + num[0] + "  до " + num[6]);
            int day = scanner.nextInt();

            if (day < 1 || day > 7) {
                throw new DaysException("Цифра не в диапозоне");
            }
            System.out.println("Наверное " + days[day - 1] + " ваш любимый день недели.");
        }
    }
}