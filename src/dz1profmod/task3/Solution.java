package dz1profmod.task3;

import java.io.*;
public class Solution {
    public static void main(String[] args) {
        BufferedReader bufferedReader = null;
        try {
            File file1 = new File("input.txt");
            File file2 = new File("output.txt");

            if (!file1.exists() && !file2.exists()) ;
            file1.createNewFile();
            file2.createNewFile();

            PrintWriter printWriter1 = new PrintWriter(file1);
            PrintWriter printWriter2 = new PrintWriter(file2);
            printWriter1.println("Hello man ");
            printWriter1.println("Hello man " + "Hello man ");
            printWriter1.println("Hello man " + "Hello man " + "Hello man ");
            printWriter1.close();

            bufferedReader = new BufferedReader(new FileReader("input.txt"));
            String line1;

            while ((line1 = bufferedReader.readLine()) != null) {
                System.out.println(line1);
                printWriter2.println(line1.toUpperCase());
            }
            printWriter2.close();

        } catch (IOException e) {
            System.out.println("Error" + e);
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                System.out.println("Error" + e);
            }
        }
    }
}
