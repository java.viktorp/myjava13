package dz1profmod.task4;

public class EvenException extends Exception{
    public EvenException(String message) {
        super(message);
    }
}
