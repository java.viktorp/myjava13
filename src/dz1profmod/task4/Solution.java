package dz1profmod.task4;

import java.util.Scanner;

public class Solution {
    public static void main(String[] args) throws EvenException {
        Scanner scanner = new Scanner(System.in);
        while (true) {
        int n = Integer.parseInt(scanner.nextLine());
            if (n % 2 != 0) {
                throw new EvenException("Ошибка. Число нечетное.");
            }
            MyEvenNumber MyPrimeNumber = new MyEvenNumber(n);
            System.out.println(MyPrimeNumber.getN());
        }
    }
}