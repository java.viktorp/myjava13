package dz1profmod.task4;

public class MyEvenNumber {
    private int n=4;

    public MyEvenNumber(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }
}
