package dz7springmod.filmography.controller;

import com.filmography.model.Director;
import com.filmography.model.Film;
import com.filmography.repository.DirectorRepository;
import com.filmography.repository.FilmRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;

@RestController
@RequestMapping("/directors")
@Tag(name = "Режиссеры", description = "Контроллер для работы с режиссерами фильмотеки")
public class DirectorController
        extends GenericController<Director> {
    public final DirectorRepository directorRepository;
    public final FilmRepository filmRepository;

    public DirectorController(DirectorRepository directorRepository, FilmRepository filmRepository) {
        super(directorRepository);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
    }

    //находим фильм, находим режиссера и к нему добавляем фильм
    @Operation(description = "Добавить фильм к режиссеру", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> addFilm(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId) {
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильма не существует по переданному id не существует" + filmId));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссера по переданному id не существует" + directorId));
        director.getFilms().add(film);
        return ResponseEntity.status(HttpStatus.CREATED).body(directorRepository.save(director));
    }
}