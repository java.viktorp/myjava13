package dz7springmod.filmography.controller;

import com.filmography.model.Director;
import com.filmography.model.Film;
import com.filmography.repository.DirectorRepository;
import com.filmography.repository.FilmRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;


@RestController
//путь запроса, который будет слушать контроллер
@RequestMapping("/films")
@Tag(name = "Фильмы", description = "Контроллер для работы с фильмотекой")
public class FilmController
        extends GenericController<Film> {
    public final FilmRepository filmRepository;
    public final DirectorRepository directorRepository;

    public FilmController(FilmRepository filmRepository, DirectorRepository directorRepository) {
        super(filmRepository);
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
    }

    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> addDirector(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId) {
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Режиссера по переданному id не существует" + filmId));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Фильма по переданному id не существует" + directorId));
        film.getDirectors().add(director);
        return ResponseEntity.status(HttpStatus.CREATED).body(filmRepository.save(film));

    }
}
