package dz7springmod.filmography.controller;

import com.filmography.model.User;
import com.filmography.repository.UserRepository;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователь", description = "Контроллер по управлению пользователями")
public class UserController
        extends GenericController<User> {
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

}
