package dz7springmod.filmography.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    //    http://localhost:8080/api/rest/swagger-ui/index.htm
    @Bean
    public OpenAPI filmographyProject() {
        return new OpenAPI()
                .info(new Info()
                        .title("Онлайн фильмотека")
                        .description("Сервис позволяющий арендовать фильмы в онлайн фильмотеке")
                        .version("v0.1")
                        .license(new License().name("Apache 2.0").url("https://springdoc.org"))
                        .contact(new Contact().name("Viktor P. z111")
                                .email("speedwayyyyy@gmail.com")
                                .url(""))
                );

    }
}
