package dz7springmod.filmography.repository;

import com.filmography.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {
}
