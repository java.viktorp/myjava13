package dz7springmod.filmography.repository;

import com.filmography.model.User;

public interface UserRepository extends GenericRepository<User> {
}
