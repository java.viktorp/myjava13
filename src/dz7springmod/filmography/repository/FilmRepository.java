package dz7springmod.filmography.repository;

import com.filmography.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Film> {
}
