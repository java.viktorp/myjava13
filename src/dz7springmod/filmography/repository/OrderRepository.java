package dz7springmod.filmography.repository;

import com.filmography.model.Order;

public interface OrderRepository extends GenericRepository<Order> {
}
