package dz7springmod.filmography.model;

import jakarta.persistence.*;

@Entity
@Table(name = "roles")
public class Role extends GenericModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;

}
