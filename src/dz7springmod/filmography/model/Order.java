package dz7springmod.filmography.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
@SequenceGenerator(name = "default_generator", sequenceName = "order_seq",
        allocationSize = 1)
@Table(name = "orders")
public class Order extends GenericModel {
    @Column(name = "rent_date", nullable = false)
    private LocalDateTime rentDate;
    @Column(name = "rent_period", nullable = false)
    private Integer rentPeriod;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDERS_USER"))
    private User user;
    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false,
            foreignKey = @ForeignKey(name = "FK_ORDERS_FILM"))
    private Film film;
}
