package dz7springmod.filmography.model;

public enum Genre {
    DRAMA("Драма"),

    NOVEL("Роман"),
    FANTASY("Фантастика");
    private final String genreTextDisplay;

    public String getGenreTextDisplay() {
        return genreTextDisplay;
    }

    Genre(String genreTextDisplay) {
        this.genreTextDisplay = genreTextDisplay;
    }
}
