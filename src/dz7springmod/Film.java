package com.filmography.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "films")
@SequenceGenerator(name = "default_generator",sequenceName = "film_seq",
        allocationSize = 1)
public class Film extends GenericModel{

    @Column(name = "title",nullable = false)
    private String title;
    @Column(name = "premier_year",nullable = false)
    private Integer premierYear;
    @Column(name = "country",nullable = false)
    private String country;
    @Column(name = "gene",nullable = false)
    @Enumerated
    private Genre gene;
    @ManyToMany
    @JoinTable(name = "films_directors",
    joinColumns = @JoinColumn(name = "film_id"),
            foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
    inverseJoinColumns = @JoinColumn(name = "director_id"),
    inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"))

    private List<Director> directors;

    @OneToMany(mappedBy = "film")
    private Set<Order>orders;


}
