package dz3profmod.task4;

import com.sun.jdi.InterfaceType;

import javax.naming.PartialResultException;
import javax.sound.midi.Soundbank;
import java.lang.annotation.Retention;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;

/*
 Написать метод, который с помощью рефлексии получит все интерфейсы
класса, включая интерфейсы от классов-родителей и интерфейсов-родителей
 */
public class Application {
    public static void main(String[] args) {
        Sportsmen sportsmen=new Sportsmen();
        Man man=new Man();
        getInterface(sportsmen,sportsmen.getClass());
    }
        static public void getInterface (Object o, Class c){
            System.out.println(Arrays.toString(c.getInterfaces()));
            System.out.println(Arrays.toString(c.getSuperclass().getInterfaces()));
            }
        }

class Man implements Sleeping,Eating{
    public void eat() {
        System.out.println("Is eat");
    }
    public void sleep() {
        System.out.println("zzz");
    }
}

class Sportsmen extends Man implements Running,Swimming{
    String name ="Egor";
    int age=15;
    public void run() {
        System.out.println("Egor is run");
    }
    public void swim() {
        System.out.println("Egor is swim");
    }
}
interface Running {
    public void run();
}
interface Swimming{
    public void swim();
}
interface Eating{
    public void eat();
}
interface Sleeping{
    public void sleep();
}