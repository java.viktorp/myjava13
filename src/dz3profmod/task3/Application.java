package dz3profmod.task3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/*
Задача: с помощью рефлексии вызвать метод print() и обработать все
возможные ошибки (в качестве аргумента передавать любое подходящее
число). При “ловле” исключений выводить на экран краткое описание ошибки.
 */
public class Application {
    public static void main(String[] args) {
        APrinter aPrinter = new APrinter();
        Class clss = aPrinter.getClass();
        Method[] methods = clss.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
            try {
                method.invoke(aPrinter, 5);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new RuntimeException(e);
            }
        }
    }
}

class APrinter {
    public void print(int a) {
        System.out.println(a);
    }
}
