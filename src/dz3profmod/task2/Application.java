package dz3profmod.task2;
import java.lang.annotation.*;
import java.util.Arrays;

/*
Написать метод, который рефлексивно проверит наличие аннотации @IsLike на
любом переданном классе и выведет значение, хранящееся в аннотации, на
экран
 */
public class Application {
    public static void main(String[] args) {
        MyClass myClass = new MyClass();
        Class clss = myClass.getClass();
        System.out.println(Arrays.toString(clss.getDeclaredAnnotations()));
    }
}
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE_USE, ElementType.CONSTRUCTOR,ElementType.FIELD})
@interface IsLike{
    boolean jump() default true;
    int a();
    String value();
}

@IsLike(a=10,value="some")
class MyClass{

}

