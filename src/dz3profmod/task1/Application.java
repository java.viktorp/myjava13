package dz3profmod.task1;
/*
Создать аннотацию @IsLike, применимую к классу во время выполнения
программы. Аннотация может хранить boolean значение.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@IsLike(value = "some",a=5)
public class Application {
}
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE_USE, ElementType.CONSTRUCTOR,ElementType.FIELD})
@interface IsLike{
    boolean jump() default true;
    int a();
    String value();
}