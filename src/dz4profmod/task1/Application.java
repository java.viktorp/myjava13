package dz4profmod.task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Посчитать сумму четных чисел в промежутке от 1 до 100 включительно и вывести ее на
экран.
 */
public class Application {
    public static void main(String[] args) {
        //Создаем лист от 1-100, реализуем  аррейлист
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }
        //Фильтруем четные числа и складываем в результат
        Integer result = list.stream().filter(n -> n % 2 == 0).reduce(0, Integer::sum); //reduce(0,(acc,n)->acc+n);
        System.out.println(result);
    }
}