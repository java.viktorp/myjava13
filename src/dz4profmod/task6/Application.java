package dz4profmod.task6;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
Дан Set<Set<Integer>>. Необходимо перевести его в Set<Integer>.
 */
public class Application {
    public static void main(String[] args) {
        Set<Integer> list1 = new HashSet<>();
        Set<Integer> list2 = new HashSet<>();
        Set<Integer> list3 = new HashSet<>();
        list1.add(12);
        list1.add(13);
        list1.add(14);
        list2.add(5);
        list2.add(6);
        list2.add(7);
        list3.add(1);
        list3.add(2);
        list3.add(3);
        Set<Set<Integer>> list = new HashSet<>();
        list.add(list1);
        list.add(list2);
        list.add(list3);

        Set<Integer> otherList = list.stream().flatMap(Collection::stream).collect(Collectors.toSet());
        otherList.forEach(System.out::println);

    }

}

