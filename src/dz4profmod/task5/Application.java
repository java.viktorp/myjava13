package dz4profmod.task5;

import java.util.List;
import java.util.stream.Stream;

/*
На вход подается список непустых строк. Необходимо привести все символы строк к
верхнему регистру и вывести их, разделяя запятой.
Например, для List.of("abc", "def", "qqq") результат будет ABC, DEF, QQQ.
 */
public class Application {
    public static void main(String[] args) {

        Stream<String> list = Stream.of("abc", "def", "qqq");
        List<String> otherlist=list.map(n->n.toUpperCase()).peek(n->System.out.print(n+",")).toList();
    }
}
