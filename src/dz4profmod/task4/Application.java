package dz4profmod.task4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

/*
На вход подается список вещественных чисел. Необходимо отсортировать их по
убыванию.
 */
public class Application {
    public static void main(String[] args) {
        Stream<Double> list = Stream.of(2.5, 4.8, 12.25, 2.4755, 16.1);
        List<Double> listsorted = list.sorted(Comparator.reverseOrder()).toList();
        listsorted.forEach(System.out::println);
    }
}
