package dz4profmod.task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

/*
На вход подается список целых чисел. Необходимо вывести результат перемножения
этих чисел.
Например, если на вход передали List.of(1, 2, 3, 4, 5), то результатом должно быть число
120 (т.к. 1 * 2 * 3 * 4 * 5 = 120).
 */
public class Application {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
       int[] arr=new int[5];
        for (int i = 0; i < 5; i++) {
            arr[i]=scanner.nextInt();
        }
//Создаем стрим из массива
        Stream<Integer>streamlist= Arrays.stream(arr).boxed();
//Считаем сумму всех элементов
        Integer result=streamlist.reduce(1,(acc,n)->acc*n);
        System.out.println(result);
    }
}

