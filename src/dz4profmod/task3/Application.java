package dz4profmod.task3;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
На вход подается список строк. Необходимо вывести количество непустых строк в
списке.
Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.

 */
public class Application {
    public static void main(String[] args) {
        Stream<String> list = Stream.of("abc", "", "", "def", "qqq");
        Long otherlist = list.filter(n -> n.length() >= 1).count();//collect(Collectors.counting());
        System.out.println(otherlist);
    }
}
